-- a.) artist that has a letter "D" in his/her name 
 SELECT * FROM artists WHERE name LIKE "%d%";
--+----+---------------+
--| ID | NAME          |
--+----+---------------+
--|  9 | Lady Gaga     |
--| 11 | Ariana Grande |
--+----+---------------+

-- b.) Songs that has less than 2:30 minutes
SELECT * FROM songs WHERE length < 230;
--+----+--------------+----------+----------------------------------------+----------+
--| id | song_name    | length   | genre                                  | album_id |
--+----+--------------+----------+----------------------------------------+----------+
--| 25 | Love Story   | 00:02:13 | Country                                |       18 |
--| 27 | Red          | 00:02:04 | Country                                |       19 |
--| 28 | Black Eyes   | 00:01:51 | Rock and Roll                          |       20 |
--| 29 | Shallow      | 00:02:01 | Country, Rock, Folk rock               |       20 |
--| 31 | Sorry        | 00:01:32 | Dancehall-poptrophical housemoombahton |       22 |
--| 34 | Thank U Next | 00:01:56 | Pop, R&B                               |       25 |
--| 35 | 24K Magic    | 00:02:07 | Funk, Disco, R&B                       |       26 |
--| 36 | Lost         | 00:01:52 | Pop                                    |       27 |
--+----+--------------+----------+----------------------------------------+----------+

-- c.) Albums and Songs join and show the title of the album and songs
SELECT album_title, song_name FROM albums JOIN songs ON albums.id = songs.album_id;
--+-----------------+----------------+
--| album_title     | song_name      |
--+-----------------+----------------+
--| Fearless        | Fearless       |
--| Fearless        | Love Story     |
--| Red             | State of Grace |
--| Red             | Red            |
--| A Star is Born  | Black Eyes     |
--| A Star is Born  | Shallow        |
--| Born This Way   | Born This Way  |
--| Purpose         | Sorry          |
--| Believe         | Boyfriend      |
--| Dangerous Woman | Into You       |
--| Dangerous Woman | Into You       |
--| Thank U, Next   | Thank U Next   |
--| 24K Magic       | 24K Magic      |
--| Earth to Mars   | Lost           |
--+-----------------+----------------+

-- d.) Join artists and albums table and fine album title that has "A" in it.
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id where album_title LIKE "%A%";
--+----+---------------+----+-----------------+---------------+-----------+
--| ID | NAME          | ID | album_title     | DATE_RELEASED | ARTIST_ID |
--+----+---------------+----+-----------------+---------------+-----------+
--|  8 | Taylor Swift  | 18 | Fearless        | 2008-11-11    |         8 |
--|  9 | Lady Gaga     | 20 | A Star is Born  | 2018-10-10    |         9 |
--|  9 | Lady Gaga     | 21 | Born This Way   | 2011-06-29    |         9 |
--| 11 | Ariana Grande | 24 | Dangerous Woman | 2016-05-20    |        11 |
--| 11 | Ariana Grande | 25 | Thank U, Next   | 2019-02-08    |        11 |
--| 12 | Bruno Mars    | 26 | 24K Magic       | 2016-11-18    |        12 |
--| 12 | Bruno Mars    | 27 | Earth to Mars   | 2011-01-28    |        12 |
--+----+---------------+----+-----------------+---------------+-----------+

 
 -- e.) Sort albums from Z-A and limit it to 4 
 SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
--+----+---------------+---------------+-----------+
--| ID | album_title   | DATE_RELEASED | ARTIST_ID |
--+----+---------------+---------------+-----------+
--| 25 | Thank U, Next | 2019-02-08    |        11 |
--| 19 | Red           | 2012-10-22    |         8 |
--| 22 | Purpose       | 2015-11-13    |        10 |
--| 18 | Fearless      | 2008-11-11    |         8 |
--+----+---------------+---------------+-----------+


-- f.) Album title z-a and Songs a-z
SELECT * FROM albums  JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;
SELECT * FROM albums  JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC;
--+----+-----------------+---------------+-----------+----+----------------+----------+----------------------------------------+----------+
--| ID | album_title     | DATE_RELEASED | ARTIST_ID | id | song_name      | length   | genre                                  | album_id |
--+----+-----------------+---------------+-----------+----+----------------+----------+----------------------------------------+----------+
--| 25 | Thank U, Next   | 2019-02-08    |        11 | 34 | Thank U Next   | 00:01:56 | Pop, R&B                               |       25 |
--| 19 | Red             | 2012-10-22    |         8 | 27 | Red            | 00:02:04 | Country                                |       19 |
--| 19 | Red             | 2012-10-22    |         8 | 26 | State of Grace | 00:02:43 | Rock, Alternative rock, Arena rock     |       19 |
--| 22 | Purpose         | 2015-11-13    |        10 | 31 | Sorry          | 00:01:32 | Dancehall-poptrophical housemoombahton |       22 |
--| 18 | Fearless        | 2008-11-11    |         8 | 24 | Fearless       | 00:02:46 | Pop rock                               |       18 |
--| 18 | Fearless        | 2008-11-11    |         8 | 25 | Love Story     | 00:02:13 | Country                                |       18 |
--| 27 | Earth to Mars   | 2011-01-28    |        12 | 36 | Lost           | 00:01:52 | Pop                                    |       27 |
--| 24 | Dangerous Woman | 2016-05-20    |        11 | 33 | Into You       | 00:02:42 | EDM House                              |       24 |
--| 21 | Born This Way   | 2011-06-29    |         9 | 30 | Born This Way  | 00:02:52 | Electropop                             |       21 |
--| 23 | Believe         | 2012-06-15    |        10 | 32 | Boyfriend      | 00:02:51 | Pop                                    |       23 |
--| 20 | A Star is Born  | 2018-10-10    |         9 | 28 | Black Eyes     | 00:01:51 | Rock and Roll                          |       20 |
--| 20 | A Star is Born  | 2018-10-10    |         9 | 29 | Shallow        | 00:02:01 | Country, Rock, Folk rock               |       20 |
--| 26 | 24K Magic       | 2016-11-18    |        12 | 35 | 24K Magic      | 00:02:07 | Funk, Disco, R&B                       |       26 |
--+----+-----------------+---------------+-----------+----+----------------+----------+----------------------------------------+----------+